define({ "api": [
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "./doc/main.js",
    "group": "D__Node_Workspace_node_apidoc_doc_main_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_doc_main_js",
    "name": ""
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/body-parser/lib/read.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_body_parser_lib_read_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_body_parser_lib_read_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/body-parser/lib/read.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_body_parser_lib_read_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_body_parser_lib_read_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/body-parser/lib/types/json.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_body_parser_lib_types_json_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_body_parser_lib_types_json_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/body-parser/lib/types/json.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_body_parser_lib_types_json_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_body_parser_lib_types_json_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/body-parser/lib/types/raw.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_body_parser_lib_types_raw_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_body_parser_lib_types_raw_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/body-parser/lib/types/text.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_body_parser_lib_types_text_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_body_parser_lib_types_text_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/body-parser/lib/types/text.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_body_parser_lib_types_text_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_body_parser_lib_types_text_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/body-parser/lib/types/urlencoded.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_body_parser_lib_types_urlencoded_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_body_parser_lib_types_urlencoded_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/body-parser/lib/types/urlencoded.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_body_parser_lib_types_urlencoded_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_body_parser_lib_types_urlencoded_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/body-parser/lib/types/urlencoded.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_body_parser_lib_types_urlencoded_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_body_parser_lib_types_urlencoded_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/content-disposition/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_content_disposition_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_content_disposition_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/content-disposition/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_content_disposition_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_content_disposition_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/content-disposition/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_content_disposition_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_content_disposition_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/content-disposition/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_content_disposition_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_content_disposition_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/content-disposition/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_content_disposition_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_content_disposition_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/content-disposition/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_content_disposition_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_content_disposition_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/content-disposition/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_content_disposition_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_content_disposition_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/content-disposition/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_content_disposition_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_content_disposition_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/content-disposition/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_content_disposition_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_content_disposition_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/content-disposition/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_content_disposition_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_content_disposition_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/cookie-signature/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_cookie_signature_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_cookie_signature_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/cookie-signature/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_cookie_signature_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_cookie_signature_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/debug/browser.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_debug_browser_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_debug_browser_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/debug/browser.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_debug_browser_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_debug_browser_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/debug/browser.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_debug_browser_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_debug_browser_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/debug/browser.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_debug_browser_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_debug_browser_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/debug/browser.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_debug_browser_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_debug_browser_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/debug/debug.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_debug_debug_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_debug_debug_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/debug/debug.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_debug_debug_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_debug_debug_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/debug/debug.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_debug_debug_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_debug_debug_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/debug/debug.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_debug_debug_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_debug_debug_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/debug/debug.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_debug_debug_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_debug_debug_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/debug/debug.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_debug_debug_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_debug_debug_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/debug/node.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_debug_node_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_debug_node_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/debug/node.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_debug_node_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_debug_node_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/debug/node.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_debug_node_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_debug_node_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/etag/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_etag_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_etag_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/express.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_express_lib_express_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_express_lib_express_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/middleware/init.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_express_lib_middleware_init_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_express_lib_middleware_init_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/middleware/query.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_express_lib_middleware_query_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_express_lib_middleware_query_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/router/layer.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_express_lib_router_layer_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_express_lib_router_layer_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/router/layer.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_express_lib_router_layer_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_express_lib_router_layer_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/router/layer.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_express_lib_router_layer_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_express_lib_router_layer_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/router/route.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_express_lib_router_route_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_express_lib_router_route_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/utils.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_express_lib_utils_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_express_lib_utils_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/utils.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_express_lib_utils_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_express_lib_utils_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/utils.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_express_lib_utils_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_express_lib_utils_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/utils.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_express_lib_utils_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_express_lib_utils_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/utils.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_express_lib_utils_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_express_lib_utils_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/utils.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_express_lib_utils_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_express_lib_utils_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/utils.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_express_lib_utils_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_express_lib_utils_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/utils.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_express_lib_utils_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_express_lib_utils_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/utils.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_express_lib_utils_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_express_lib_utils_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/utils.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_express_lib_utils_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_express_lib_utils_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/utils.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_express_lib_utils_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_express_lib_utils_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/utils.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_express_lib_utils_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_express_lib_utils_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/utils.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_express_lib_utils_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_express_lib_utils_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/express/lib/utils.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_express_lib_utils_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_express_lib_utils_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/fresh/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_fresh_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_fresh_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/media-typer/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_media_typer_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_media_typer_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/media-typer/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_media_typer_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_media_typer_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/media-typer/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_media_typer_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_media_typer_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/media-typer/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_media_typer_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_media_typer_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/media-typer/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_media_typer_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_media_typer_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/ms/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_ms_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_ms_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/ms/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_ms_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_ms_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/ms/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_ms_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_ms_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/ms/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_ms_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_ms_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/on-headers/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_on_headers_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_on_headers_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/path-to-regexp/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_path_to_regexp_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_path_to_regexp_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/proxy-addr/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_proxy_addr_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_proxy_addr_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/proxy-addr/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_proxy_addr_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_proxy_addr_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/proxy-addr/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_proxy_addr_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_proxy_addr_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/proxy-addr/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_proxy_addr_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_proxy_addr_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/proxy-addr/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_proxy_addr_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_proxy_addr_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/proxy-addr/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_proxy_addr_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_proxy_addr_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/proxy-addr/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_proxy_addr_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_proxy_addr_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/proxy-addr/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_proxy_addr_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_proxy_addr_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/proxy-addr/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_proxy_addr_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_proxy_addr_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/proxy-addr/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_proxy_addr_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_proxy_addr_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_send_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/serve-favicon/node_modules/ms/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_serve_favicon_node_modules_ms_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_serve_favicon_node_modules_ms_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/serve-favicon/node_modules/ms/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_serve_favicon_node_modules_ms_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_serve_favicon_node_modules_ms_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/serve-favicon/node_modules/ms/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_serve_favicon_node_modules_ms_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_serve_favicon_node_modules_ms_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/serve-favicon/node_modules/ms/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_serve_favicon_node_modules_ms_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_serve_favicon_node_modules_ms_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/serve-static/node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/serve-static/node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/serve-static/node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/serve-static/node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/serve-static/node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/serve-static/node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/serve-static/node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/serve-static/node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/serve-static/node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/serve-static/node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/serve-static/node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/serve-static/node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/serve-static/node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/serve-static/node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/serve-static/node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/serve-static/node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/serve-static/node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/serve-static/node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/serve-static/node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/serve-static/node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/serve-static/node_modules/send/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_serve_static_node_modules_send_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/utils-merge/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_utils_merge_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_utils_merge_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/vary/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_vary_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_vary_index_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/vary/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_vary_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_vary_index_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "./node_modules/vary/index.js",
    "group": "D__Node_Workspace_node_apidoc_node_modules_vary_index_js",
    "groupTitle": "D__Node_Workspace_node_apidoc_node_modules_vary_index_js",
    "name": "Public"
  },
  {
    "type": "get",
    "url": "/users",
    "title": "List all users",
    "group": "Tasks",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "tasks",
            "description": "<p>Task's list</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n[{\n  \"id\": 1,\n  \"title\": \"Study\",\n  \"done\": false\n  \"updated_at\": \"2016-02-10T15:46:51.778Z\",\n  \"created_at\": \"2016-02-10T15:46:51.778Z\"\n}]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "List error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/users.js",
    "groupTitle": "Tasks",
    "name": "GetUsers"
  }
] });
